@extends('layouts.app')

@section('title')
    Home
@endsection

{--
    At a minimum, this view should have a search bar to find particular titles and a "Browse" button that will bring the user to the catalogue of books on offer. 
    --}